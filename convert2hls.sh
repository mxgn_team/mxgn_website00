#!/bin/env bash

ffmpeg -i video.mp4 -profile:v baseline -level 3.0 -start_number 0 -hls_time 1 -hls_list_size 0 -f hls index.m3u8
